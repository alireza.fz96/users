import { Component, OnInit } from '@angular/core';
import { User } from '../../../models/user.model';
import { UsersService } from '../../../services/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  users: User[];
  currentPage = 0;
  totalPages: number;

  constructor(private usersService: UsersService) {}

  ngOnInit() {
    this.fetchUsers();
  }

  fetchUsers() {
    this.usersService.fetchUsers(this.currentPage).subscribe((data) => {
      this.users = data.users;
      if (!this.totalPages) this.totalPages = data.totalPages;
    });
  }

  onPageChange() {
    this.fetchUsers();
  }
}
