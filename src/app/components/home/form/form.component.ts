import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../services/users.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
  constructor(private usersService: UsersService) {}

  ngOnInit(): void {}
  onAddUser(userForm: NgForm) {
    this.usersService.addUser(userForm.value);
  }
}
