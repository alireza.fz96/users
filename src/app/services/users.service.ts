import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  private users: User[];

  constructor(private http: HttpClient) {}

  getUsers() {
    return this.users.slice();
  }

  getUserById(id: number): User {
    return this.users.find((user) => user.id === id);
  }

  fetchUsers(pageNumber = 0) {
    return this.http
      .get<{ data: User[]; total_pages: number }>(
        'https://reqres.in/api/users',
        {
          params: { per_page: '5', page: pageNumber + '' },
        }
      )
      .pipe(
        map((res) => {
          this.users = res.data;
          return { users: this.users, totalPages: res.total_pages };
        })
      );
  }

  addUser(user: User) {
    this.http
      .post('https://reqres.in/api/users', user)
      .subscribe((res) => console.log(res));
  }
}
